#!/bin/bash

bucket=`curl https://api.bitbucket.org/2.0/repositories/%7Bf2ff309c-c45c-4d58-9ad1-235d6d8cfdd0%7D`
lab=`curl -s https://gitlab.com/api/v4/users/tbodica/projects`
hub=`curl -s https://api.github.com/users/p0licat/repos`

echo $bucket | grep -o "https://bitbucket.org/tbodica/[^\"]*" | grep -o "https://bitbucket.org/tbodica/[^\"]*" | grep "\.git"
echo $lab | grep  --color "web_url\":\"[^,]*\"\," -o
echo $hub | grep "clone_url\": \"[^\"]*" -o | grep -o "[^ \"]*$"
