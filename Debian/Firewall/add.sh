#!/bin/bash
ADDR=$1

sudo iptables -I OUTPUT 1 -d $ADDR -j ACCEPT
sudo iptables -I INPUT 1 -s $ADDR -j ACCEPT
