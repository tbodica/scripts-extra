sudo iptables -P FORWARD DROP
sudo iptables -P INPUT DROP
sudo iptables -P OUTPUT DROP


sudo iptables -A INPUT -i lo 	-j ACCEPT
sudo iptables -A OUTPUT -o lo 	-j ACCEPT

sudo iptables -A INPUT 		-j LOG --log-level 4	--log-prefix "***INPUT DROP***"
sudo iptables -A OUTPUT		-j LOG --log-level 4 	--log-prefix "***OUTPUT DROP***"
sudo iptables -A FORWARD	-j LOG --log-level 4 	--log-prefix "***FORWARD DROP***"


sudo iptables -A INPUT		-j DROP
sudo iptables -A OUTPUT		-j DROP
sudo iptables -A FORWARD	-j DROP
