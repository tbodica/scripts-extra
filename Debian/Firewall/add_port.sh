#!/bin/bash

usage="$(basename "$0") [0:65535] [tcp/udp]"

if [ -z "$1" ]
	then
		echo "Please insert port."
		echo $usage
		exit 1
fi

if [ -z "$2" ]
	then
		echo "Please insert protocol (TCP/UDP)."
		echo $usage
		exit 1
fi

re_port='^[0-9]+$'
if ! [[ $1 =~ $re_port ]] ; then
	echo "Must be an integer."
	exit 1
fi

if [[ "$2" != "TCP" && "$2" != "tcp" && "$2" != "udp" && "$2" != "UDP" ]] ; then
	echo "Please specify protocol."
	echo $usage
	exit 1
fi

# initializations
PV=$1
PP=$2

if sudo iptables -C OUTPUT -p $PP --sport $PV -j ACCEPT 2> /dev/null; then
	echo "Command already exists as a rule. Exitting..."
	exit 1
fi

if sudo iptables -C INPUT -p $PP --dport $PV -j ACCEPT 2> /dev/null; then
	echo "Command already exists as a rule. Exitting..."
	exit 1
fi


if sudo iptables -C INPUT -p $PP --sport $PV -j ACCEPT 2> /dev/null ; then
	echo "Command already exists as a rule. Exitting..."
	exit 1
fi

if sudo iptables -C OUTPUT -p $PP --dport $PV -j ACCEPT 2> /dev/null; then
	echo "Command already exists as a rule. Exitting..."
	exit 1
fi

sudo iptables -I OUTPUT -p $PP --sport $PV -j ACCEPT
sudo iptables -I INPUT -p $PP --dport $PV -j ACCEPT
sudo iptables -I OUTPUT -p $PP --dport $PV -j ACCEPT
sudo iptables -I INPUT -p $PP --sport $PV -j ACCEPT

echo "Done."
