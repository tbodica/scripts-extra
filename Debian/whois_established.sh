#!/bin/bash

if [ -z "$1" ]
	then 
		echo "Please insert left side of ip to match."
		exit 1
fi

QUERY=$1
echo "Looking for ips with $QUERY on the left side."


IPaddr=`sudo netstat -tpn | egrep "$QUERY[.0-9]*[^ :]" --color -o | head -n 1`

echo "Matches: $IPaddr"
echo ""

echo "Performing whois on $IPaddr"
WHOIS_OUT=`whois $IPaddr`

echo ""
echo "$WHOIS_OUT" | grep "Organization:"
echo "$WHOIS_OUT" | grep "NetRange:"
