import sys


from Modules.TextUI import TextUserInterface, ReadInteger, GetArguments
from Modules.Text import Text


class Controller(object):
    def __init__(self, text):
        self._text = text
        self._version_int = int(sys.version.split()[0].split('.')[0])

    def MostCommonList(self):
        firstParam = ReadInteger("Insert number of words to search:")
        secondParam = ReadInteger("Insert lower length bound?:")
        return self._text.MostCommonList(firstParam, secondParam)

    def PrintNthWord(self):
        a = raw_input("Enter N: ") if self._version_int == 2 else input("Enter N: ")
        try:
            a = int(a)
        except Exception as e:
            print(e)
            return
        return self._text.PrintNthWord(a)

    def WordSequence(self):
        a = raw_input("Enter lower bound: ") if self._version_int == 2 else input("Enter lower bound: ")
        b = raw_input("Enter lower bound: ") if self._version_int == 2 else input("Enter lower bound: ")

        try:
            a = int(a)
            b = int(b)
        except Exception as e:
            print(e)
            return
        return self._text.WordSequence(a, b)

    def SortedWords(self):
        print(self._text.SortedWords())
        return self._text.SortedWords()


    def GraphOccurencesToLength(self):
        return self._text.GraphOccurencesToLength()

    def GraphWordLengthDensity(self):
        return self._text.GraphWordLengthDensity()

    def GraphTextWordLength(self):
        return self._text.GraphTextWordLength()

    def ContextPermutationPrint(self):
        minv = ReadInteger("Insert minimum value:")
        cntv = ReadInteger("Insert count:")
        return self._text.ContextPermutationPrint(minv, cntv)

    def SubstringMatching(self):
        return self._text.HateYourselfSubstringMatchingSequenceOnBytes()

    def SubstringMatchingOnWords(self):
        return self._text.SubstringMatchingOnWords()

    def PrintNumberOfWords(self):
        nwords = self._text.GetNumberOfWords()
        print(nwords)
        return nwords

def main():

    fn = GetArguments('filename')

    tt = Text(fn) # warper should be ok not to modify
    ct = Controller(tt)
    ui = TextUserInterface([
                                "1 - Graph occurences to length.",
                                "2 - Most common words.",
                                "3 - Graph word length density.",
                                "4 - Graph whole text, length with index.",
                                "5 - Context permutations.",
                                "6 - Substring matching.",
                                "7 - Substring matching on words.",
                                "8 - Print Nth word.",
                                "9 - Print sequence of words.",
                                "10 - Print words sorted by length.",
								"11 - Number of words."
                            ],
                            [
                                (ct.GraphOccurencesToLength, None),
                                (ct.MostCommonList, None),
                                (ct.GraphWordLengthDensity, None),
                                (ct.GraphTextWordLength, None),
                                (ct.ContextPermutationPrint, None),
                                (ct.SubstringMatching, None),
                                (ct.SubstringMatchingOnWords, None),
                                (ct.PrintNthWord, None),#tt.PrintNthWord
                                (ct.WordSequence, None),
                                (ct.SortedWords, None),
								(ct.PrintNumberOfWords, None)
                            ],
                            "Choose option: ",
                            "Exitting...")

    ui.MainLoop()
    tt.GetFO().close()

main()
