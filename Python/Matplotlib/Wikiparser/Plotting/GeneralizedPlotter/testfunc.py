import sys
import math
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import axes3d

def FillMatrix(dlist):
    density_matrix = []
    print(dlist)
    """
    for line in range(len(dlist)):
        words = dlist[line].split(' ') # space = 0
        wnums = []

        for word in words:
            nword = [k for k in range(len(word))]
            revkw = [k for k in range(len(word))[::-1]]
            wnums.append([1 + min(nword[i], revkw[i]) for i in range(len(nword))])
            # math wnums.append(
                   
        #print(wnums)
        mline = []
        for i in wnums:
            for k in i:
                mline.append(k)
            mline.append(0)
        density_matrix.append(mline[:-1])
        density_matrix.append(mline[:-1])

    print(np.mat(density_matrix))
    """
    words = ""
    for i in dlist:
        for car in i:
            words+=car
    words = words.split(' ') 
    wnums = []
    for word in words:
        nword = [k for k in range(len(word))]
        revkw = [k for k in range(len(word))[::-1]]
        wnums.append([1 + min(nword[i], revkw[i]) for i in range(len(nword))])
        
    fline = []
    for i in wnums:
        for k in i:
            fline.append(k)
        fline.append(0)
    print(words)
    print(fline)
    lc = 0
    for i in range(len(dlist)):
        cline = []
        for k in range(len(dlist[0])):
            cline.append(fline[lc*len(dlist[0]) + k])
        density_matrix.append(cline)
        lc += 1
    print(density_matrix)
    return density_matrix

def WireframePlotMatrix(dmat):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = np.linspace(0, 48, 48)
    y = np.linspace(0, 48, 48)

    X, Y = np.meshgrid(x, y)
   
    print(X)
    print(len(Y))
    print(len(np.array(dmat)))

    ax.plot_wireframe(X, Y, np.array(dmat), rstride=10, cstride=10)
    plt.show()

def SurfacePlotMatrix(dmat):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    r = np.arange(0, 4.8, 0.1)
    p = np.arange(0, 32.2, 0.1)

    R, P = np.meshgrid(r, p)
    

    ax.plot_surface(P, R, 0.1 * np.array(dmat), cmap=plt.cm.YlGnBu_r, vmin = 0, vmax = 1)
    plt.show()

def PrintSquareList(dlist):
    for i in dlist:
        print(i)

def TopMap(dlist):
    density_matrix = []

    upmat = []
    dnmat = []
    
    linecount = len(dlist)

    for line in range(linecount):
        words = dlist[line].split(' ')
        wnums = []

        for word in words:
            # palindrome generation
            nword = [k for k in range(len(word))]
            revkw = [k for k in range(len(word))[::-1]]
            wnums.append([1 + min(nword[i], revkw[i]) for i in range(len(nword))])
            
        mline = []
        for i in wnums:
            for k in i:
                mline.append(k)
            mline.append(0)
        mline = mline[:-1] # mline is filled palindrome line

        # normal factor is without *10 and dec -1
        dnmat.append(mline[:])
        for i in range(max(mline)* 10):
            dnmat.append(mline[:])
        # up and down matrix
        for i in range(max(mline)* 10):
            upmat.append(mline[:])
        upmat.append(mline[:])
        
        for i in range(len(upmat)-2, -1, -1):
            for j in range(len(upmat[i])):
                upmat[i][j] = upmat[i+1][j] - 0.1 if upmat[i+1][j] > 0 else upmat[i+1][j]

        for i in range(1, len(dnmat)):
            for j in range(len(dnmat[i])):
                dnmat[i][j] = dnmat[i-1][j] - 0.1 if dnmat[i-1][j] > 0 else dnmat[i-1][j]
        
        for i in upmat:
            density_matrix.append(i)
        for i in dnmat:
            density_matrix.append(i)

        upmat = []
        dnmat = []
        
    print("Density matrix:")
    print(np.mat(density_matrix))
    print("Length of mat:")
    print(len(np.mat(density_matrix)))
    print("End output.")
    return density_matrix

def SurfaceMap2(rval):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    r = np.arange(0, 6)
    p = np.arange(0, 6)

    R, P = np.meshgrid(r, p)
    ax.plot_surface(R, P, np.array(rval), cmap=plt.cm.YlGnBu_r, vmin = 0, vmax = 4)

    plt.show()
def SurfaceMapNormalized(rval):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    p = np.arange(0, len(rval))
    r = np.arange(0, len(rval[0]))

    R, P = np.meshgrid(r, p)

    maxelement = 0
    for i in rval:
        for k in i:
            maxelement = max(maxelement, k) 
    ax.plot_surface(R, P, np.array(rval), cmap=plt.cm.coolwarm)
    ax.set_zlim(0, 6.2*maxelement)
    #plt.show()
    plt.savefig('file.png')

def SurfaceMapSquare(rval):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    r = np.arange(0, len(rval[0]))
    p = np.arange(0, len(rval))

    R, P = np.meshgrid(r, p)

    maxelement = 10
    for i in rval:
        for j in i:
            maxelement = max(maxelement, j)

    print(np.array(rval).ndim)
    print("NPAR")
    #ax.plot_surface(R, P, np.array(rval), cmap=plt.cm.YlGnBu_r)
    ax.plot_surface(R, P, np.array(rval), alpha=0.3)
    ax.set_zlim(-10, maxelement)
    cset = ax.contour(R, P, np.array(rval), zdir='z', offset=-10, cmap=cm.coolwarm)
    plt.show()


def FillMatrix2(elist):
    # transform 1line ; find x|y ; return filled matrix

    superstring = ""
    for i in elist:
        if i != '\n':
            superstring += i
        else:
            superstring += " "

    x = len(superstring)
    # 2y^2 < x | 2y < sqrt(x) | y < sqrt(x)/2 ? >=!
    y = int(math.sqrt(x/2.+1))
    print("Len: {0}".format(len(superstring))) 
    ccount = 0
    lcount = 0
    clist = ""
    dlist = []
    print("Superstring is:{0}".format(superstring))
    for i in superstring:
        if ccount == 2*y:
            ccount = 0
            lcount += 1
            dlist.append(clist)
            print("Appended: {0}".format(clist))
            clist = ""
        clist += i
        ccount += 1
    

    print("Count is: {0}".format(lcount))
    for i in dlist:
        print("X: {0}".format(len(i)))
    return FillMatrix(dlist)
 
def main3():
    ffnam = sys.argv[1]
    od = open(ffnam, 'r')
    
    dlist = []
    elist = od.read()

    od.seek(0)
    """
    for i in range(24):
        dlist.append(od.read(48))
    od.close()
    """
    #rval = FillMatrix(dlist)
    rval = FillMatrix2(elist)
    #SurfaceMapSquare(rval)
    print(len(rval))
    print(len(rval[0]))
    
    #SurfaceMapSquare(rval)
    SurfaceMapNormalized(rval) 


main3()

