## wikiparser

![screenshot](./Resources/Images/wiki.png)
![screen2](./Plotting/GeneralizedPlotter/file.png)

Data visualizer for articles.

## Usage:

Still in very early development, it's possible to use the ui from running:
`python pyspace.py /path/to/file`
and choose an option from there.

For the 3d plot, use Plotting/testfunc.py.

## Depends:

Uses `matplotlib`.
