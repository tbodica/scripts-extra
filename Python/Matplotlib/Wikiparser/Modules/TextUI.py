import sys

from Modules.TypeEvaluation import FunctionType, MethodType

def GetArguments(vstr=""):
	if vstr == "filename":
		fn = sys.argv[1] if len(sys.argv) == 2 else None
		if fn is None:
			print("Need filename as argument.")
			raise Exception("Not a filename.")
		return fn
	
	raise Exception("Need a valid option.")
				

def ReadInteger(message=""):
	ires = None
	try:
		ires = input(message)
	except Exception as ex:
		print(ex)

	try:
		ires = int(ires)
	except Exception as ex:
		print(ex)

	return ires if type(ires) is int else None
	

class TextUserInterface(object):
    
    def __init__(self, menuOptions, callAddresses, inputQuery="", loopCompleteString = ""):
        """
        todo: tuple list of menuOptions with functions addr
        number = list item
        0 = exit
        """

        self.menuOptions = menuOptions
        self.callAddresses = callAddresses
        self.inputQuery = inputQuery
        self.loopCompleteString = loopCompleteString
        
        def fs():
            return 3

        class tc:
            def fsc():
                return 1

        if type(callAddresses) is not list:
            raise Exception(type(callAddresses))
        if type(menuOptions) is not list:
            raise Exception("Not a list.")

        if len(menuOptions) != len(callAddresses):
            raise Exception("Labels and functions must have same number.")

        for func_tuple in self.callAddresses:
            func = func_tuple[0]
            func_args = func_tuple[1]
            if type(func) is not type(fs) and type(func) is not type(tc.fsc) and type(func) is not FunctionType() and type(func) is not MethodType(): 
                print(type(func))
                print(type(fs))
                raise Exception("Call addresses corrupted.")

            if type(func_args) is not list and func_args is not None:
                print(func_args)
                print(type(func_args))
                raise Exception("Maybe list.")

 
    def MainLoop(self):

        while True:
            print("Menu options:")
            for option in self.menuOptions:
                print(option)

            print("Input \'0\' to exit.")
            inputResult = raw_input(self.inputQuery) if int(sys.version.split()[0].split('.')[0]) == 2 else input(self.inputQuery)
            print(inputResult)
            print(type(inputResult))
            if inputResult == '0' or inputResult == 0:
                print(self.loopCompleteString)
                return True     # EventComplete class?

            try:
                inputResult = int(inputResult)
            except ValueError as ve:
                print(ve)
                print("Cannot forget to treat this exception.")
                print("Please use integers to navigate menu.")
                continue

            print(type(inputResult))
            
            if inputResult > len(self.callAddresses):
                print("Number is too large.")
                continue
            if inputResult < 0:
                print("Number can not be negative.")
                continue
            #todo:
            #tuples
            #del if
            rval = self.callAddresses[inputResult-1][0]() if self.callAddresses[inputResult-1][1] is None else self.callAddresses[inputResult-1][0](self.callAddresses[inputResult-1][1])
            print("Call returned: " + str(rval))
            print("\n")

