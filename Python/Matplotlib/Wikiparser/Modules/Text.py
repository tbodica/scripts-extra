import sys
from matplotlib import pyplot

from Modules.StringParsing import WhitelistStripping

# todo: refactor prints, abstract class, DB and trees.
# module: abstract class, interface(cline, ipy)... regex
# DB, sources, crawling... wiki?
# substring matching for lexicon prefixes & suffixes
# count optimization of whitespace removal on bytes algorithm
# lower length substrings for phonology
# palindromes for occipital non-phonos
# unsupervised learning, feature extraction, learning
class Text:
    def __init__(self, filename):
        self.fileSize = None
        self.words = None
        self.fileName = filename
        self.bytes = None
        self.fo = None
        self.strippedWords = []

        self.numbersToWordsRatio = None

        try:
            self.fo = file(self.fileName) if int(sys.version.split()[0].split('.')[0]) == 2 else open(self.fileName)
        except IOError:
            print("Filename error.")
            return

        self.fo = file(self.fileName) if int(sys.version.split()[0].split('.')[0]) == 2 else open(self.fileName) # python
        self.bytes = self.fo.read()
        self.words = self.bytes.split()
        for i in self.words:
            self.strippedWords.append(WhitelistStripping(i))

    def __del__(self):
        self.fo.close()

    def GetNumberOfWords(self):
        return len(self.words)
 
    def AlphabeticWordTree(self, alphaChar):
        text = self.strippedWords
        rlist = list()

        for i in text:
            if i not in rlist:
                rlist.append(i)

        rlist.sort()
        wtree = []

        for i in rlist:
            if len(i) >= 1 and i[0] == alphaChar:
                wtree.append(i)

        print (wtree)
        return wtree

    def GraphWordLengthDensity(self):
        wbase = self.strippedWords
        uniqdict = dict()
        for i in wbase:
            b = WhitelistStripping(i)
            uniqdict[b] = 1 if b not in uniqdict else uniqdict[b] + 1
        len_occ_dict = dict()
        for i in uniqdict:
            len_occ_dict[len(i)+1] = uniqdict[i] if len(i)+1 not in len_occ_dict else uniqdict[i] + len_occ_dict[len(i)+1]

        pyplot.plot(len_occ_dict.keys(), len_occ_dict.values())
        pyplot.ylabel("occurences")
        pyplot.xlabel("length of word")
        pyplot.show()

    def GraphOccurencesToLength(self):
        occurenceDict = dict()
        for i in self.words:
            i = WhitelistStripping(i)
            occurenceDict[i] = 1 if i not in occurenceDict else occurenceDict[i] + 1

        notUniqueList = []
        for i in occurenceDict:
            if occurenceDict[i] > 1:
                notUniqueList.append((occurenceDict[i], i))

        notUniqueList.sort()

        xplot = [i for i in range(notUniqueList[-1:][0][0]+1)]
        yplot = [0 for k in range(len(xplot))]

        for i in notUniqueList:
            yplot[i[0]] = len(i[1])

        pyplot.scatter(xplot, yplot)
        pyplot.plot(xplot, yplot)

        pyplot.ylabel("length of word occuring more than once")
        pyplot.xlabel("occurences")
        pyplot.show()

    def GetNumbersToWordsRatio(self):

        if self.numbersToWordsRatio is not None:
            return self.numbersToWordsRatio

        text = self.words
        num="1234567890"
        alp="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

        numbers = []
        words = []

        for i in text:
            typeflag = ""
            for k in range(len(i)):
                if i[k] in num:
                    typeflag += "n"
                elif i[k] in alp:
                    typeflag += "a"
                else:
                    typeflag += "!"

            while typeflag[-1:] == "!":
                i = i[:-1]
                typeflag = typeflag[:-1]

            if "!" not in typeflag:
                if "a" not in typeflag:
                    numbers.append(i)
                elif "n" not in typeflag:
                    words.append(i)

        self.numbersToWordsRatio = float(len(numbers)) / float(len(words))
        return self.numbersToWordsRatio

    def ContextPermutationPrint(self, minl, count):
        if count >= len(self.strippedWords):
            return None

        f = 0
        for i in range(count, len(self.strippedWords)):
            f = 1
            for k in range(count):
                if len(self.strippedWords[i-k]) < minl:
                    f = 0
                    break
            if f == 0:
                continue

            beautyList = []
            for k in range(count)[::-1]:
                beautyList.append(self.strippedWords[i - k])

            print(beautyList)

    def GraphTextWordLength(self):
        xaxis = [i for i in range(len(self.strippedWords))]
        yaxis = [len(k) for k in self.strippedWords]

        print(self.strippedWords)
        count = 0
        for i in self.strippedWords:
            count += 1
            print(str(count) + " : " + str(i) + " " + str(len(i)))

        pyplot.plot(xaxis, yaxis)
        pyplot.xlabel("index of word")
        pyplot.ylabel("length of word")
        pyplot.show()

    def SubstringMatchingOnWords(self):
        pass 

    def HateYourselfSubstringMatchingSequenceOnBytes(self, onWords=False):
        # todo: n=grams?
        def CountMatches(matchString, sstr):
            #"starving for food and cannabis edition, praise all-seeing eyes"
            count = 0
            # time 1
            i = 0
            j = len(matchString)
            # time 2

            #print("Matching algorithm for: ")
            #print(matchString)
            #print(sstr)
            #print("for loop:")
            while j != len(sstr) + 1:
                if matchString == sstr[i:j]:
                    count += 1
                #print(sstr[i:j])
                i += 1
                j += 1
                #t3


            return count

        sstr = self.bytes
        occDict = dict()

        for k in range(len(sstr)):
            print(k)
            countMatch = 0
            for i in range(k, len(sstr)):
                matchString = sstr[k:i+1]
                if matchString in occDict:
                    continue
                countMatch = CountMatches(matchString, sstr)
                #print("COUTN MATCH: " + str(countMatch))
                occDict[matchString] = max(occDict[matchString], countMatch) if matchString in occDict else countMatch

        occDoub = [(occDict[i], i) for i in occDict if occDict[i] >= 2]
        print(occDoub)
        occDoub.sort()
        print(occDoub)

    def MostCommonList(self, l, minWordLength):
        """
           l - most common l words
           minWordLength - lower length bound
        """
        def sortTupleListWithCountValues(tupleListWithCountValues):
            """
                a list of tuples : [(a, b), (etc, etc), (word, 11), (word, count)]
                bubble sort flags
                Will sort tuple list by count (second tuple element).
            """
            done = False
            while not done:
                done = True
                for i in range(1, len(tupleListWithCountValues)):
                    if tupleListWithCountValues[i-1][1] > tupleListWithCountValues[i][1]:
                        tupleListWithCountValues[i-1] , tupleListWithCountValues[i] = tupleListWithCountValues[i] , tupleListWithCountValues[i-1]
                        done = False

        t = self.words
        li = []
        countey = dict()
        for i in t:
            i = WhitelistStripping(i)
            if i not in countey:
                countey[i] = 0
            else:
                countey[i] += 1
        # dict of stripped words mapped to count exists here (countey)

        # parse countey, generate list and keep sorted
        for i in countey.items():
            if len(li) < l and len(i[0]) >= minWordLength:
                li.append(i)
            elif len(li) >= l and len(i[0]) >= minWordLength:
                sortTupleListWithCountValues(li)
                if li[0][1] < i[1]:
                    li[0] = i
                sortTupleListWithCountValues(li)

        # return sorted list of unique (word, count)
        print(li)
        return li
   
    #todo
    #bounds
    #categories // , data models, etc
    #
    # 1. article(wiki)->paragraphs->sentences, etc
    # 2. html

    def SortedWords(self):
        rval = []
        for i in self.words:
            if (len(i), i) not in rval:
                rval.append((len(i), i))
        
        rval.sort()
        rrval = [k[1] for k in rval]

        return rrval
    
    def PrintNthWord(self, n):
        return self.words[n]

    def WordSequence(self, lb, ub):
        return self.words[lb:ub]

    def GetFO(self):
        return self.fo

    def GetBytes(self):
        return self.bytes

    def GetWords(self):
        return self.words

    def PrintText(self):
        print(self.bytes)


