import DomainHTMLParser.UrlController as dctl


def main():

    # read homepage from homepage.txt
    homeUrl = ""
    with open('homepage.txt', 'r') as fd:
        homeUrl = fd.readlines()

    homeUrl = str(homeUrl[0])
    # end


    dc = dctl.DomainChecker()
    dc.setHomePage(homeUrl)
    dc.crawlDomain()
    dc.printAllAccessedURLs()


main()