import urllib
import urllib.request

import re

class UrlOperations:

    @staticmethod
    def GetHTMLData(url_string):
        response = urllib.request.urlopen(url_string)
        return response.read()

    @staticmethod
    def SearchDomainUrlsInData(data, domainName):
        """
            Return urls belonging to domain that are found in page data.
            Returns a dict(). 
        """
        domain_left = domainName.split('.')[0]
        domain_right = domainName.split('.')[1]

        pat = r'http[s]?\:\/\/{0}\.{1}\/[^ \'\"<\)]*'.format(domain_left, domain_right)
        urlMatches = re.findall(pat, data.decode())
        
        matches = dict()
        for url_match in urlMatches:
            matches[url_match] = 1

        return matches
        


class DomainChecker:
    def __init__(self):
        self.__homePage = str()
        self.__accessedUrlRepo = dict()
        self.__inaccessibleUrlRepo = dict()

    def crawlDomain(self):
        """
            Main crawler entry point.
        """
        homePageData = UrlOperations.GetHTMLData(self.__homePage)
        self.__accessedUrlRepo[self.__homePage] = 1

        uniqueDomainUrlDict = UrlOperations.SearchDomainUrlsInData(homePageData, 'wikipedia.org')
        
        domainChanged = True
        while domainChanged:
            domainChanged = False
            for domainUrl in uniqueDomainUrlDict.keys():
                if domainUrl in self.__accessedUrlRepo or domainUrl in self.__inaccessibleUrlRepo:
                    continue
                
                print(domainUrl)

                try:
                    domainUrlData = UrlOperations.GetHTMLData(domainUrl)
                except urllib.error.HTTPError:
                    self.__inaccessibleUrlRepo[domainUrl] = 1


                self.__accessedUrlRepo[domainUrl] = 1
                
                try:
                    newUniqueDomainUrlDict = UrlOperations.SearchDomainUrlsInData(domainUrlData, 'wikipedia.org')
                except UnicodeDecodeError:
                    if 'jpg' in domainUrl.split('.')[-1]:
                        continue

                for k in newUniqueDomainUrlDict.keys():
                    uniqueDomainUrlDict[k] = 1
                domainChanged = True
                break


    # getters & setters
    def setHomePage(self, homePage):
        self.__homePage = homePage


    def getHomePage(self):
        return self.__homePage
    # end getters & setters

    # console printing
    def printAllAccessedURLs(self):
        for url_entry in self.__accessedUrlRepo.keys():
            print(url_entry)

    # end console printing