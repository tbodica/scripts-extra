import urllib
import urllib.request

import re

class DomainHtmlParser:
    def __init__(self, urlpath=None):
        """
            Initialize object and get HTML GET data.

            params:
                self.homePage 
                self.data
                self.urlpath
                self.urlMatches
                self.matchNotAccessibleList
        """
        self.homePage = 'http://wikipedia.org'
        
        self.urlpath = None
        self.data = None

        if urlpath != None:
            self.urlpath = self.homePage + '/' + urlpath.lstrip('/')
            

        if urlpath == None:
            response = urllib.request.urlopen(self.homePage)
        else:
            print(self.urlpath)
            response = urllib.request.urlopen(self.urlpath)

        self.data = response.read()

        self.urlMatches = list()
        self.matchNotAccessibleList = list()


    def getPatternErrors(self):
        """
            Get list of inaccessible links.
        """
        return self.matchNotAccessibleList if self.matchNotAccessibleList != [] else False


    def QueryMatches(self):
        """
            Try to GET all matched url paths, found in self.urlMatches
            Inaccessible WPDM files' urls are stored in matchNotAccessibleList.
        """
        for item in self.urlMatches:
            print("Trying to access: {0}".format(item))
            data = urllib.request.urlopen(item)
            data = data.read()
            if len(data) < 100:
                self.matchNotAccessibleList.append(item)


    def searchPattern(self, pattern):
        """
            Search a pattern in urls found in the current page.
        """
        self.urlMatches = re.findall(r'http\:\/\/.*\?{0}'.format(pattern), self.data.decode() )
        return self.urlMatches


    def searchDomainURLs(self):
        """
            Search URLs in current page.
        """
        self.urlMatches = re.findall(r'http[s]?\:\/\/wikipedia\.org\/[^ \'\"]*', self.data.decode())


    def printHtmlData(self):
        print(self.data)

