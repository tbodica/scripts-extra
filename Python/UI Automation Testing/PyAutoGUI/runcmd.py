import pyautogui as pag
import time

def type_word(word=None, press_enter=False):
    if word == None or not isinstance(word, str):
        return

    for c in word:
        if c == ' ':
            pag.press('space')
            continue
        pag.press(c)

    if press_enter:
        pag.press('enter')

def clear_textbox(start_place='beginning'):
    starting_places = [
        'beginning',
        'end',
        'all'
    ]
    if start_place not in starting_places:
        return

    if start_place == 'all':
        pag.keyDown('ctrl')
        pag.press('a')
        pag.keyUp('ctrl')
        pag.press('del')

    if start_place == 'beginning':
        pag.keyDown('shift')
        pag.press('end')
        pag.keyUp('shift')
        pag.press('del')

    if start_place == 'end':
        pag.keyDown('shift')
        pag.press('home')
        pag.keyUp('shift')
        pag.press('del')

def open_run_window():
    pag.keyDown('win')
    pag.press('r')
    pag.keyUp('win')

def open_cmd_window(method='runwindow'):
    if method.rstrip() == 'runwindow':
        open_run_window()
        location = None
        try:
            location = pag.locateOnScreen('./screenshots/run_tool.PNG')
        except:
            print("Image not found on screen.")
            return

        print('location found: {0}'.format(str(location)))
        # 72, 80 is relative position of text box
        pag.click(location.left + 75, location.top + 80)
        print('clicked in textbox')

        # select all to the right and delete phase
        pag.keyDown('shift')
        pag.press('end')
        pag.keyUp('shift')
        # end last phase

        # type command to run phase
        pag.press('c')
        pag.press('m')
        pag.press('d')
        pag.press('enter')
        # end last phase

def run_window(command='cmd'):
    open_run_window()
    location = None
    try:
        time.sleep(1)
        location = pag.locateOnScreen('./screenshots/run_tool_2.PNG')
        assert location != None
    except:
        print("Image not found on screen.")
        raise Exception("nfi")

    # relative location is
    pag.click(location.left + 62, location.top + 70)

    clear_textbox(start_place='all')
    type_word(command, press_enter=True)


def test1(max_tries = 3):
    #open_cmd_window(method='runwindow') # equivalent to run_window()
    for counter in range(max_tries):
        try:
            run_window('notepad')
        except:
            continue
        print("Took {0} (re)tries.".format(str(counter)))
        break

#test1()

def click_on_icon(max_tries = 3, icon_name=None):
    if icon_name is None:
        return False

    for counter in range(max_tries):
        try:
            x, y = pag.locateCenterOnScreen('./screenshots/{0}'.format(str(icon_name)))
            pag.click(x, y)
        except:
            print('Something went wrong clicking the icon. Retrying...')
            break
        print("Took {0} (re)tries.".format(str(counter)))
        break

    return True


def compact_mails(max_tries = 3):
    # open Control Panel phase
    success = False
    for counter in range(max_tries):
        try:
            run_window('Control Panel')
            success = True
        except:
            continue
        print("Took {0} (re)tries.".format(str(counter)))
        break
    # end phase
    if not success:
        return

    # search Mail in Control Panel phase
    success = False
    for counter in range(max_tries):
        try:

            # 1124, 57 relative position of text box
            #location = pag.locateCenterOnScreen('./screenshots/search_control_panel_2.PNG')
            #pag.click(location.left + 20, location.top + 8)
            #print('Found control panel search box.')
            time.sleep(3)
            pag.keyDown('ctrl')
            pag.press('f')
            pag.keyUp('ctrl')
            type_word('Mail')
            success = True

            #find next Icon phase
            #end phase

        except:
            print("Image not found. Retrying...")
            continue
        print("Took {0} (re)tries.".format(str(counter)))
        break

    if not success:
        return
    # end phase

    # click on mail icon phase
    success = False
    for counter in range(max_tries):
        try:
            time.sleep(1)
            x, y = pag.locateCenterOnScreen('./screenshots/search_mail_icon.PNG')
            pag.click(x, y)
            success = True
        except:
            print('Something went wrong clicking mail. Retrying...')
            break
        print("Took {0} (re)tries.".format(str(counter)))
        break
    if not success:
        return
    # end phase

    # click on data files phase
    success = False
    for counter in range(max_tries):
        try:
            time.sleep(5)
            x, y = pag.locateCenterOnScreen('./screenshots/data_files_icon.PNG')
            print('debug reached')
            pag.click(x  + 300, y)
            print('debug reached 2')
            success = True
        except:
            print('Something went wrong clicking data files. Retrying...')
            break
        print("Took {0} (re)tries.".format(str(counter)))
        break
    if not success:
        return
    # end phase

    # select active mail icon phase

    # this fails
    # if not click_on_icon(icon_name='active_mail_icon.PNG'):
    #     print("Did not click on icon....")
    #     return

    # press tab 7 times
    #
    # go to listview
    time.sleep(3)
    for _i in range(7):
        pag.press('tab')

    max_dbs = 2
    for _i in range(max_dbs):

        # for _t in range(10):
        #     pag.press('down')

        if click_on_icon(icon_name='settings_icon.PNG'):
            break


    time.sleep(1)
    for _t in range(4):
        pag.press('tab')
    pag.press('enter')

    print("Done.")

compact_mails()
