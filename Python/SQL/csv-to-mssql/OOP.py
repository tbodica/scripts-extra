import hashlib
import csv
import datetime
import re


class Quantity:
    def __init__(self, row):
        print(row)
        self.allowedWeights = [
            'g',
            'GR',
            'L',
            'ML',
            'ml',
            'l',
            'gr',
            'kg'
        ]
        
        weightValue = None
        weightType = ""
        multiplier = None

        m = re.search(r"[0-9]?\.?[0-9]+(g|GR|L|ML|ml|l|gr|kg)", row)
        if m:
            weightValue = m.group(0)

            m = re.search(r"[0-9]+(x|X)", row)
            if m:
                multiplier = m.group(0)
        else:
            m = re.search(r"[0-9]+(x|X)", row)
            if m:
                multiplier = m.group(0)

        fStr = "(" + "".join([str(k) + "|" for k in self.allowedWeights]).rstrip('|') + ")"
        m = re.search(fStr, row)
        if m:
            weightType = m.group(0)
            weightValue = weightValue.split(weightType)[0]

        self.multiplier = multiplier.rstrip("X").rstrip('x') if multiplier is not None else ""
        self.unit = weightType if weightType is not None else ""
        self.weight = weightValue if weightValue is not None else ""

    def __str__(self):
        return "Quantity{" + "weight={0}".format(self.weight) + self.unit + " X {0}".format(self.multiplier) + "}"


class Item:
    def __init__(self, 
        name, price, 
        quantity, date, store
    ):
        self.name = name
        self.price = price
        self.quantity = quantity
        self.date = datetime.datetime.strptime(date, '%d.%m.%Y %H:%M:%S')
        self.store = store

    def GetDateHash(self):
        m = hashlib.md5()
        m.update(str(self.date).encode('ascii'))
        return m.hexdigest()[3:10]


    def __str__(self):
        return "Item{" + self.name + ", " + self.price + ", " + str(self.quantity) + "}"




class SourceDataFile:
    def __init__(self, srcFile):
        self.srcFile = srcFile
        
        self.distinctItemsQuery = "INSERT INTO TemporaryItems(i_name, price) VALUES "
        self.shoppingCartsQuery = "INSERT INTO ShoppingCarts(transaction_code, closed, i_id, multiplier, quantity, unit) VALUES"
        self.transactionsQuery = "INSERT INTO Transactions(cashless, price, time_of_transaction, c_id, s_id, sh_id) VALUES"

        self.Items = list()
        self.ShoppingCarts = list()
        self.Transactions = list()


    def GetItems(self):
        with open(self.srcFile, newline='') as csvfile:
            items_file = csv.reader(csvfile, delimiter=',', quotechar='|')

            for row in items_file:
                name = row[0]
                quantity = Quantity(row[1])
                price = row[2]
                date = row[4]
                store = row[5]

                newItem = Item(
                    name, 
                    price, 
                    quantity,
                    date,
                    store
                )
                self.Items.append(newItem)


    def GetShoppingCarts(self):
        for item in self.Items:
            self.ShoppingCarts.append((
                item.GetDateHash(),
                'FALSE',
                item.name,
                item.quantity
            ))
    
  
    def GetTransactions(self):
        auxCart = list()

        currentCode = ""
        transactionSum = 0.0
        ct = 0
        currentDateTime = None
        currentStoreName = None

        for item in self.Items:

            auxCart.append((
                item.GetDateHash(),
                'NULL',
                item.name,
                str(item.date)
            ))

            if currentCode == "":
                currentCode = item.GetDateHash()
                currentDateTime = str(item.date)
                currentStoreName = item.store

            if item.GetDateHash() == currentCode:
                transactionSum += float(item.price)
                ct += 1
            else:
                self.Transactions.append((
                    transactionSum,
                    str(item.date),
                    1,
                    item.store,
                    currentCode
                ))
                ct = 0
                currentCode = item.GetDateHash()
                transactionSum = float(item.price)
                currentCode = item.GetDateHash()
                currentDateTime = str(item.date)
                currentStoreName = str(item.store)

        self.Transactions.append((
                transactionSum,
                str(item.date),
                1,
                item.store,
                currentCode
            ))   

    def Extract(self):
        self.GetItems()
        self.GetShoppingCarts()
        self.GetTransactions()

    

    def WriteShoppingCarts(self, output):
        with open(output, 'w') as wd:
            for item in self.ShoppingCarts:

                selectQ = """
                (
                    SELECT MIN(i_id) FROM Items
                    WHERE Items.i_name LIKE \'%{0}%\'
                )
                """.format(item[2])
                itemsString = [
                    item[0], 
                    item[1], 
                    [selectQ], 
                    str(item[3].multiplier),
                    str(item[3].weight),
                    str(item[3].unit)
                ]

                self.shoppingCartsQuery += PrintingHelper(itemsString).GetFormattedString()
                self.shoppingCartsQuery += "\n"
            self.shoppingCartsQuery.rstrip().rstrip(",")
            wd.write(self.shoppingCartsQuery)

    def WriteDistinctItems(self, output):
        with open(output, 'w') as wd:
            for item in self.Items:
                itemsString = [item.name, item.price]
                self.distinctItemsQuery += PrintingHelper(itemsString).GetFormattedString()
            
            self.distinctItemsQuery.rstrip().rstrip(',')
            wd.write(self.distinctItemsQuery)

    def WriteTransactions(self, output):
        with open(output, 'w') as wd:
            for t in self.Transactions:
                sidQ = """(
                    SELECT s_id from Stores
                    WHERE Stores.Name LIKE '%{0}%'
                )
                """.format(t[3])
                kidQ = """(
                    SELECT MIN(s_id) FROM ShoppingCarts
                    WHERE ShoppingCarts.transaction_code LIKE '%{0}%'
                )
                """.format(t[4])
                itemsString = [
                    'TRUE',
                    str(t[0]), # price
                    str(t[1]), # date
                    str(t[2]), # clientId
                    [str(sidQ)], # storeId
                    [str(kidQ)]  # trCode
                    ]

                self.transactionsQuery += PrintingHelper(itemsString).GetFormattedString()
            self.transactionsQuery.rstrip().rstrip(',')
            wd.write(self.transactionsQuery)

            
    def PrintItemsDebug(self):
        for item in self.Items:
            print(item)

    def PrintShoppingCartsDebug(self):
        for i in self.ShoppingCarts:
            print(i)

    def PrintTransactionsDebug(self):
        for i in self.Transactions:
            print(i)
                



class PrintingHelper:
    def __init__(self, strings):
        self.listOfStrings = strings

    def GetFormattedString(self):
        rVal = "("

        for string in self.listOfStrings:
            if isinstance(string, list):
                rVal += string[0] + ','
            else:
                rVal += '\''
                rVal += string
                rVal += '\'' + ','

        rVal = rVal.rstrip(',')
        rVal += '),'
        rVal += "\n"

        return rVal
            


sourceData = SourceDataFile("./Export_12_12_2020.csv")

sourceData.Extract()

sourceData.PrintItemsDebug()
sourceData.PrintShoppingCartsDebug()
sourceData.PrintTransactionsDebug()

sourceData.WriteDistinctItems('distinctItems.txt')
sourceData.WriteShoppingCarts('shoppingCarts.txt')
sourceData.WriteTransactions('allTransactions.txt')

