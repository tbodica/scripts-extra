import poplib
import sys
from email import parser

def mail_connection(username=None, password=None):
    pop_conn = poplib.POP3_SSL('pop.gmail.com') # replace without SSL
    pop_conn.user(username)
    pop_conn.pass_(password)
    return pop_conn

def fetch_mail(username, password):
    pop_conn = mail_connection(username, password)
    messages = [pop_conn.retr(i) for i in range(1, 10)]
    messages = ["\n".join(mssg[1]) for mssg in messages]
    messages = [parser.Parser().parsestr(mssg) for mssg in messages]

    pop_conn.quit()
    return messages

def get_attachments(username, password):
    messages = fetch_mail(username, password)
    attachments = []
    for msg in messages:
        print(msg)
        for part in msg.walk():
            name = part.get_filename()
            data = part.get_payload(decode=True)
            if name is None:
                continue
            f = open(name, 'wb')
            f.write(data)
            f.close()
            attachments.append(name)
    return attachments


password = sys.argv[1]
get_attachments('tbodica@gmail.com', password) # app key used

