import discord
import datetime
import psycopg2 as psycop

bot_name = "ActivityBot"
help_str = """\n
stackoverflow @user
youtube @user
music @user
"""


class Bot1(discord.Client):
    async def msg_send(self, content, message):
        await message.channel.send(content)

    async def on_ready(self):
        boot_str = str(datetime.datetime.now())
        print("Boot on {0}".format(boot_str))

    async def on_message(self, message):
        print(message.mentions)
        if message.author == self.user:
            return
        if bot_name in message.content or bot_name in [k.name for k in message.mentions]:
            if len(message.content.split(' ')) > 3:
                await message.channel.send(help_str)
                return

            if "stackoverflow" in message.content:

                conn_string = "host='localhost' dbname='browsing' user='postgres' password=password='{0}'".format(password_string)
                conn = psycop.connect(conn_string)

                cur = conn.cursor()
                cur.execute("SELECT * FROM stackoverflow")
                tbuff = []
                for k in cur.fetchall():
                    tbuff.append(k)
                cur.close()
                conn.close()

                sendmsg = "List, last 15: \n"
                for j in tbuff[:15]:
                    sendmsg += str(j[2]) + "\n"

                print(sendmsg)

                await self.msg_send("Stackoverflow history for " + message.content.split(' ')[2] + ":\n" + sendmsg, message)
                return

            if "youtube" in message.content:
                await self.msg_send("Not implemented: youtube history for " + message.content.split(' ')[2], message)
                return

            if "music" in message.content:
                await self.msg_send("Not implemented: music history for " + message.content.split(' ')[2], message)
                return

            await message.channel.send(help_str)


if __name__ == "__main__":
    client = Bot1()
    client.run('{0}'.format(discord_key))