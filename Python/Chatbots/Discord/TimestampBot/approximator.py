import pickle
from datetime import datetime, timedelta


with open('filename.pickle', 'rb') as handle:
    b = pickle.load(handle)

dt = []
for c in b:
    v = datetime.strptime(c.split('.')[0], '%Y-%m-%d %H:%M:%S')
    dt.append(v)


time_spent = 0
current_time_spent = 0
delta_spec = timedelta(minutes=15)
for i in range(1, len(dt)):
    ct = dt[i]
    ct_i = dt[i-1]


    delta = ct_i - ct

    if (delta) < delta_spec:
        current_time_spent += delta.total_seconds()

    else:
        time_spent += current_time_spent
        current_time_spent = 0


print(time_spent)
print(time_spent)