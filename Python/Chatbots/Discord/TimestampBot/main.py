import discord
import datetime
import pickle
#import psycopg2 as psycop

bot_name = "ActivityBot"


class Bot1(discord.Client):
    async def on_ready(self):
        boot_str = str(datetime.datetime.now())
        print("Boot on {0}".format(boot_str))

        text_channel_list = self.get_all_channels()
        channel = None
        for channel_it in text_channel_list:
            if (str(channel_it) == "general"):
                channel = channel_it

        messages = await channel.history(limit=10000).flatten()
        msg_str = []
        print("Found: {0} messages".format(len(messages)))

        for m in messages:
            print(m)
            print(type(m))

            msg_str.append(str(m.created_at))

        print(msg_str)

        with open('filename.pickle', 'wb') as handle:
            pickle.dump(msg_str, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == "__main__":
    discord_key = "[SENSITIVE]"
    client = Bot1()
    client.run('{0}'.format(discord_key))
